<?php
Class Blog_model extends MY_Model{
	function get_categories()
	{
		$query = $this->db->get('demo_blog_categories');
		return $query->result();
	}
	function add_new_entry($name,$body,$categories)
    {
        $data = array(
			'author_id'=>2,
            'title' => $name,
            'content' => $body,
			'category_id' => $categories
        );
        $this->db->insert('demo_blog_posts',$data);
    }
	function get_photos()
	{
		$query = $this->db->get('demo_cover_photos');
		return $query->result();
	}
}

?>