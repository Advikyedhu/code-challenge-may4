<script>
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.
	
      var map, infoWindow;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 1.290270, lng: 103.851959},
          zoom: 6
        });
        infoWindow = new google.maps.InfoWindow;
		
		google.maps.event.addDomListener(window, "resize", function() {
		
		  var center = map.getCenter();

		  resizeMap();

		  google.maps.event.trigger(map, "resize");
		  map.setCenter(center);

		});

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }
	  function resizeMap(){
		  var element = document.getElementById('map');
		  var h = element.offsetHeight;
		  var w = element.offsetWidth;

		  //var h = window.innerHeight;
		  //var w = window.innerWidth;
			
		  $("#map").width(w-350);
		  $("#map").height(h-50);

	}
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUg-e6XBGB78CiWiPZKE9oRDToBjo-A0c&callback=initMap" type="text/javascript">
    </script>
<div class="container-fluid">
	
	 <div id="smartwrap" width="100%" >
	 <div id="author-img"><h5><b>Meeting with Yedhu</b></h5></div>
		  <div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 " >
					<div class='meet'>
						<div class="icon_time"><img src="http://[::1]/codingchallenge/assets/images/icon_time.png"/><span class='lpad-txt'>Starts   </span>			
						</div>
						<div class="time-lpad">
						<span ><script type="text/javascript">
						document.write (' <span id="date-time">', new Date().toLocaleString(), '<\/span>')
						if (document.getElementById) onload = function () {
							setInterval ("document.getElementById ('date-time').firstChild.data = new Date().toLocaleString()", 50)
						}
						</script></span>
						</div>
					</div>
				</div>
					<div class="col-md-5 col-sm-5 col-xs-5" >
						<div class="location">
							<div class="icon_time"><img src="http://[::1]/codingchallenge/assets/images/icon_location.png"/><span class='lpad-txt'>Location   </span>			
							</div>
							<div class="location-lpad">
								Raffles Place
							</div>
						</div>
					</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 " >
					<div class="second">
						<div class='meet  meet-mtop'>
							<div class="icon_time"><img src="http://[::1]/codingchallenge/assets/images/icon_invitees.png"/><span class='lpad-txt'>Invitees   </span>			
							</div>
						</div>
					</div>
			    </div>
				<div class="col-md-5 col-sm-5 col-xs-5 " >
					<div id="note">
						<div class="icon_time"><img src="http://[::1]/codingchallenge/assets/images/icon_note.png"/><span class='lpad-txt'>Note   </span>			
							</div>
							<div class="note-lpad">
								<div id="note-size">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
							</div>
					</div>
				</div>				
			</div>
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 " >	
					<div id="music">
						<div id="author-img"><img class="circular--square" src="http://[::1]/codingchallenge/assets/images/david.png" width=40 height=40/>
							<span class="music-lpad">
								<audio controls>
								<source src="horse.ogg" type="audio/ogg">
								<source src="http://[::1]/codingchallenge/assets/images/music.mp3" type="audio/mpeg">
								</audio>
						   </span>
						</div>
		
					</div>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5 " >	
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 " >
					<div class="second ">
						<div class='meet smart-top'>
							<div class="icon_time"><img src="http://[::1]/codingchallenge/assets/images/icon_smartlife.png"/><span class='lpad-txt'>Smartlife   </span>			
							</div>
						</div>
					</div>
			    </div>
				<div class="col-md-5 col-sm-5 col-xs-5 ">
					<div id='smartimage'><img class="img-responsive" src="http://[::1]/codingchallenge/assets/images/box-image.jpg"/></div>
				</div>
							
			</div>
			<div class="row">
				<div class="col-md-5 col-sm-5 col-xs-5 map-top " >
					<div id="map">
					</div>
				</div>
			</div>	
		</div>
	</div>	
			
			
