	<h1>PHP and Codeignter<h1>
<h2>Requirements</h2>
<p><strong>Task 1: </strong>
<ol>
	<li>Using the existing PHP and codeigniter codes for blogs within this application (you may choose to use your own method), create a controller that allows user to add_blog post into the database.</li>
	<li>You should write the controller, model and view for the add_blog function</li>
	<li>Add a button on this page that allows the user to go to the add_blog page</li>
	<li>The inserted blog record should be shown on this page</li>
</ol>
<h4><a href="Blog/add_new_entry">Add Blog</a></h4>
<!--Start Coding-->

<!--End Coding-->

<?php foreach ($posts as $post): ?>
	<table class="table table-striped table-bordered">
		<tr>
			<th>Title: </th>
			<td><a href="demo/blog_post/<?php echo $post->id; ?>"><?php echo $post->title; ?></a></td>
		</tr>
		<tr>
			<th>Content Brief: </th>
			<td><?php echo $post->content_brief; ?></td>
		</tr>
		<tr>
			<th>Publish Time: </th>
			<td><?php echo $post->publish_time; ?></td>
		</tr>
		<tr>
			<th>Author: </th>
			<td><?php echo $post->author->first_name; ?> <?php echo $post->author->last_name; ?></td>
		</tr>
		<tr>
			<th>Category: </th>
			<td><?php echo $post->category->title; ?></td>
		</tr>
		<tr>
			<th>Tags: </th>
			<td>
				<?php $count_tags = count($post->tags); ?>
				<?php for ($i=0; $i<$count_tags; $i++): ?>
					<?php echo ($i<$count_tags-1) ? $post->tags[$i]->title.',' : $post->tags[$i]->title; ?>
				<?php endfor; ?>
			</td>
		</tr>
	</table>
	<hr/>
<?php endforeach; ?>

<div class="row text-center">
	<div class="col col-md-12">
		<p>Results: <strong><?php echo $counts['from_num']; ?></strong> to <strong><?php echo $counts['to_num']; ?></strong> (total <strong><?php echo $counts['total_num']; ?></strong> results)</p>
		<?php echo $pagination; ?>
	</div>
</div>