 <script>
 alert("map");
      // Note: This example requires that you consent to location sharing when
      // prompted by your browser. If you see the error "The Geolocation service
      // failed.", it means you probably did not give permission for the browser to
      // locate you.
      var map, infoWindow;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 1.290270, lng: 103.851959},
          zoom: 6
        });
        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUg-e6XBGB78CiWiPZKE9oRDToBjo-A0c&callback=initMap" type="text/javascript">
    </script>
<div id="smartwrap" width="768" height="1200">
	
	<div class='first' >
		<span ><h5>Meeting with Yedhu</h5></span>
		<div class='meet'>
			<div id="icon_time"><img src="http://[::1]/codingchallenge/assets/images/icon_time.png"/><span>Starts   </span>			
			</div>
			<div class="time-lpad">
			<span ><script type="text/javascript">
			document.write (' <span id="date-time">', new Date().toLocaleString(), '<\/span>')
			if (document.getElementById) onload = function () {
				setInterval ("document.getElementById ('date-time').firstChild.data = new Date().toLocaleString()", 50)
			}
			</script></span>
			</div>
		</div>
		<div class="location">
			<div class="icon_time"><img src="http://[::1]/codingchallenge/assets/images/icon_location.png"/><span>Location   </span>			
			</div>
			<div class="time-lpad">
				Raffles Place
			</div>
		</div>
	</div>
	<div class='second'>
		<div class='meet'>
		    <div class="icon_time"><img src="http://[::1]/codingchallenge/assets/images/icon_invitees.png"/><span>Invitees   </span>			
			</div>
		</div>
		<div id="note"></div>
	</div>
	
	<div id="music">
		<div style="position:relative;bottom:150px;left:30px;float:left"><img class="circular--square" src="http://[::1]/codingchallenge/assets/images/david.png" width=40 height=40/><audio controls>
  <source src="horse.ogg" type="audio/ogg">
  <source src="horse.mp3" type="audio/mpeg">

</audio></div>
		
	</div>
	<div id="third">
		<div class='meet'></div>
		<div id='smartimage'></div>
	</div>
	<div id="map">
	</div>
</div>