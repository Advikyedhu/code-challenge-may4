
  <h4>Add new entry</h4>
  <?php echo validation_errors(); ?>
  <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>
  <?php  echo form_open('Blog/add_new_entry');?>
  
    <p ><label>Title: </label>
    <span style="padding-left:45px;"><input type="text" name="entry_name" size="32" /></span></p>
 
    
   <p> <label>Your Entry: </label>
    <textarea rows="7" cols="33%" name="entry_body" style="resize:none;"></textarea></p>
    
	<p><label>Categories: </label>
    <?php if( isset($categories) && $categories): foreach($categories as $category): ?>
        <label><input class="radio" type="radio" name="entry_category" value="<?php echo $category->id;?>"><?php echo $category->title;?></label>
        <?php endforeach; else:?>
        Please add your category first!
        <?php endif; ?>
    </p>
    <input class="button" type="submit" value="Submit"/>
    <input class="button" type="reset" value="Reset"/>    
 
</form>
  <?php echo form_close();?>
