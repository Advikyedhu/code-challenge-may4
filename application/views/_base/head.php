<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	
	<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>" />	
	<link rel="stylesheet" href="<?php echo base_url("assets/css/style.css"); ?>" />
	<link rel="stylesheet" href="<?php echo base_url("assets/css/jcarousel.basic.css"); ?>" />		
	<script src="<?php echo base_url("assets/js/jquery.js"); ?>" ></script>
	<script src="<?php echo base_url("assets/js/jquery.jcarousel.min.js"); ?>" ></script>
	<script src="<?php echo base_url("assets/js/jcarousel.basic.js"); ?>" ></script>
	<base href="<?php echo $base_url; ?>" />
	<style>
	td
	{border-left:1px solid black;
	border-top:1px solid black;}
	th
	{border-left:1px solid black;
	border-top:1px solid black;}
	table
	{border-right:1px solid black;
	border-bottom:1px solid black;}
</style>
	<title><?php echo $page_title; ?></title>

	<?php
		foreach ($meta_data as $name => $content)
		{
			echo "<meta name='$name' content='$content'>".PHP_EOL;
		}

		foreach ($stylesheets as $media => $files)
		{
			foreach ($files as $file)
			{
				$url = starts_with($file, 'http') ? $file : base_url($file);
				echo "<link href='$url' rel='stylesheet' media='$media'>".PHP_EOL;	
			}
		}
		
		foreach ($scripts['head'] as $file)
		{
			$url = starts_with($file, 'http') ? $file : base_url($file);
			echo "<script src='$url'></script>".PHP_EOL;
		}
	?>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="<?php echo $body_class; ?>">